//
//  DetailViewController.swift
//  Countries
//
//  Created by Krystin Stutesman on 7/19/16.
//  Copyright © 2016 Krystin Stutesman. All rights reserved.
//

import UIKit

class DetailViewController : UIViewController {
    
    var country: Country?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var capitalLabel: UILabel!
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var populationLabel: UILabel!
    @IBOutlet weak var currenciesLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let country = country {
            self.nameLabel.text = country.name
            self.capitalLabel.text = country.capital
            self.regionLabel.text = country.region
            self.populationLabel.text = String(country.population)
            self.currenciesLabel.text = country.currencies.joinWithSeparator(", ")
        }
    }
}