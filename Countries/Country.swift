//
//  Country.swift
//  Countries
//
//  Created by Krystin Stutesman on 7/19/16.
//  Copyright © 2016 Krystin Stutesman. All rights reserved.
//

import Foundation

struct Country {
    let name: String
    let capital: String
    let region: String
    let population: Int
    let currencies: [String]
}