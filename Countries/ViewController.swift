//
//  ViewController.swift
//  Countries
//
//  Created by Krystin Stutesman on 7/19/16.
//  Copyright © 2016 Krystin Stutesman. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {
    
    let countrySearchUrl = "https://restcountries.eu/rest/v1/all"
    var allCountries = [Country]()
    var filteredCountries = [Country]()
    var selectedIndex = NSIndexPath()

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var countryNumberLabel: UILabel!
    @IBOutlet weak var noResultsLabel: UILabel!
    
    // NOTE: I did this because I was going to do a separate web request every time the user searched
    // Now a bit unnecessary since I am only showing the Loading View on load but still fun :)
    var isBusy: Bool = false {
        didSet {
            showLoadingView(isBusy)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isBusy = true
        getAllCountries() { (response) in
            self.allCountries = response
            self.filteredCountries = response
            
            // NOTE: Felt strange putting this dispatch here but needed to run UI updates on main thread...
            dispatch_async(dispatch_get_main_queue(),{
                self.tableView.reloadData()
                self.isBusy = false
                self.countryNumberLabel.text = String(self.filteredCountries.count)
            })
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.deselectRowAtIndexPath(selectedIndex, animated: false)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetailSegue" {
            let destinationVC = segue.destinationViewController as! DetailViewController
            destinationVC.country = self.filteredCountries[self.selectedIndex.row]
        }
    }
    
    // MARK - Search
    
    // NOTE: I wanted to hook into the search bar's clear button so that I could endEditing on clear but it would be kinda messy
    // Probably should have just used a UISearchController but I wanted to try it out with the TableView
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        self.search(searchText)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        let searchText = searchBar.text!
        search(searchText)
    }
    
    func search(searchString: String) {
        if searchString == "" {
            filteredCountries = allCountries
        } else {
            filteredCountries = allCountries.filter {
                $0.name.lowercaseString.containsString(searchString.lowercaseString)
            }
        }

        // NOTE: I do this in two places - didn't think it was worth breaking out into a method
        self.tableView.reloadData()
        self.countryNumberLabel.text = String(self.filteredCountries.count)
    }

    // MARK - Helpers
    @IBAction func viewTapped(sender: AnyObject) {
        self.self.view.endEditing(true)
    }
    
    func getAllCountries(callback: ([Country]) -> ()) {
        let requestURL: NSURL = NSURL(string: countrySearchUrl)!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: requestURL)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest) {
            (data, response, error) -> Void in
            
            let httpResponse = response as! NSHTTPURLResponse
            let statusCode = httpResponse.statusCode
            
            if (statusCode == 200) {
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options:.AllowFragments)
                    
                    if let countries = json as? [[String: AnyObject]] {
                        var countriesArray = [Country]()
                        for country in countries {
                            
                            guard let name = country["name"] as? String,
                                let capital = country["capital"] as? String,
                                let region = country["region"] as? String,
                                let population = country["population"] as? Int,
                                let currencies = country["currencies"] as? [String]
                                else {
                                    throw NSError(domain: NSURLErrorDomain, code: NSURLError.BadServerResponse.rawValue, userInfo: nil)
                            }
                            
                            let newCountry = Country(name: name, capital: capital, region: region, population: population, currencies: currencies)
                            countriesArray.append(newCountry)
                        }
                        callback(countriesArray)
                    } else {
                        throw NSError(domain: NSURLErrorDomain, code: NSURLError.BadServerResponse.rawValue, userInfo: nil)
                    }
                } catch {
                    self.showErrorAlert("\(error)")
                }
            } else {
                self.showErrorAlert("Status code: \(statusCode)")
            }
        }

        task.resume()
    }
    
    func showErrorAlert(message: String) {
        let alert = UIAlertController(title: "Error. Please try again", message: message, preferredStyle: .Alert)
        let closeAction = UIAlertAction(title: "Okay", style: .Default, handler: { UIAlertAction in
            self.dismissViewControllerAnimated(true, completion: nil)
        })
        alert.addAction(closeAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func showLoadingView(shouldShow: Bool) {
        if shouldShow {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
        
        UIView.animateWithDuration(0.3, animations: {
            self.loadingView.alpha = shouldShow ? 0.5 : 0
            self.loadingView.hidden = shouldShow
        })
    }
    
    // MARK - TableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // NOTE: Lazy hiding of tableView if no results
        self.tableView.hidden = (filteredCountries.count == 0) ? true : false
        self.noResultsLabel.hidden = (filteredCountries.count == 0) ? false : true
        return self.filteredCountries.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("countryCell")! as! CountryCell
        cell.nameLabel.text = self.filteredCountries[indexPath.row].name
        cell.regionLabel.text = self.filteredCountries[indexPath.row].region
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedIndex = indexPath
        self.view.endEditing(true)
        self.performSegueWithIdentifier("showDetailSegue", sender: self)
    }
}

