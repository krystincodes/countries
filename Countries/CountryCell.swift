//
//  CountryCell.swift
//  Countries
//
//  Created by Krystin Stutesman on 7/19/16.
//  Copyright © 2016 Krystin Stutesman. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var regionLabel: UILabel!
}